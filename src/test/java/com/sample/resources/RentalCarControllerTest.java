package com.sample.resources;

import com.sample.exceptions.RentalCarException;
import com.sample.model.Car;
import com.sample.model.CarMetaData;
import com.sample.model.Perdayrent;
import com.sample.model.request.Metrics;
import com.sample.model.request.RentalCarRequest;
import com.sample.model.request.RentalCount;
import com.sample.model.response.RentalCarResponse;
import com.sample.services.PriceService;
import com.sample.services.RevenueService;
import com.sample.services.TeslaService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RentalCarControllerTest {

    @InjectMocks private RentalCarController controller;
    @Mock private TeslaService teslaService;
    @Mock private PriceService priceService;
    @Mock private RevenueService revenueService;
    @Mock private RentalCarRequest rentalCarRequest;

    private List<Car> carList = null;

    @Before
    public void setUp() throws Exception {
        Car car = new Car();
        car.setMake("BMW");
        car.setModel("X3");
        car.setVin("123");

        CarMetaData metaData = new CarMetaData();
        metaData.setColor("blue");
        metaData.setNotes("notes");
        car.setMetadata(metaData);

        Metrics metrics = new Metrics();
        metrics.setDepreciation(12.23F);
        metrics.setYoymaintenancecost(23.45F);

        RentalCount rentalCount = new RentalCount();
        rentalCount.setLastweek(1);
        rentalCount.setYeartodate(20);

        metrics.setRentalCount(rentalCount);
        car.setMetrics(metrics);

        Perdayrent rent = new Perdayrent();
        rent.setDiscount(10F);
        rent.setPrice(50F);
        car.setPerdayrent(rent);

        carList = new ArrayList<>();
        carList.add(car);

        when(rentalCarRequest.getCars()).thenReturn(carList);
    }

    @Test
    public void testTeslaCars_successful() throws Exception {
        when(teslaService.findTeslaCars(anyListOf(Car.class))).thenReturn(carList);

        RentalCarResponse result = controller.teslaCars(rentalCarRequest);

        verify(teslaService).findTeslaCars(carList);
        assertEquals(carList, result.getBlueTeslas());
        verifyZeroInteractions(priceService);
        verifyZeroInteractions(revenueService);
    }

    @Test
    public void testTeslaCars_throwsException() throws Exception {
        when(teslaService.findTeslaCars(anyListOf(Car.class))).thenThrow(new RentalCarException(123, "testException"));

        try {
            controller.teslaCars(rentalCarRequest);
            fail("Should have thrown exception");
        } catch (RentalCarException e) {
            verify(teslaService).findTeslaCars(carList);
            assertEquals(123, e.getErrorId());
            verifyZeroInteractions(priceService);
            verifyZeroInteractions(revenueService);
        }
    }

    @Test
    public void testLowprice_successful() throws Exception {
        when(priceService.findLowestPriceCars(anyListOf(Car.class))).thenReturn(carList);
        when(priceService.findLowestPriceDiscountedCars(anyListOf(Car.class))).thenReturn(carList);

        RentalCarResponse result = controller.lowprice(rentalCarRequest);

        verify(priceService).findLowestPriceCars(carList);
        verify(priceService).findLowestPriceDiscountedCars(carList);

        assertEquals(carList, result.getLowestPriceCars());
        assertEquals(carList, result.getLowestPriceDiscountedCars());
        assertNull(result.getBlueTeslas());

        verifyZeroInteractions(teslaService);
        verifyZeroInteractions(revenueService);
    }

    @Test
    public void testLowprice_throwsException() throws Exception {
        when(priceService.findLowestPriceCars(anyListOf(Car.class))).thenReturn(carList);
        when(priceService.findLowestPriceDiscountedCars(anyListOf(Car.class))).thenThrow(new RentalCarException(123, "testException"));

        try {
            controller.lowprice(rentalCarRequest);
            fail("Should have thrown exception");
        } catch (RentalCarException e) {
            assertEquals(123, e.getErrorId());

            verify(priceService).findLowestPriceCars(carList);
            verify(priceService).findLowestPriceDiscountedCars(carList);
            verifyZeroInteractions(teslaService);
            verifyZeroInteractions(revenueService);
        }
    }

    @Test
    public void testLowestprice_successful() throws Exception {
        when(priceService.findLowestPriceCars(anyListOf(Car.class))).thenReturn(carList);

        RentalCarResponse result = controller.lowestprice(rentalCarRequest);

        verify(priceService, times(1)).findLowestPriceCars(carList);

        assertEquals(carList, result.getLowestPriceCars());
        assertNull(result.getLowestPriceDiscountedCars());
        assertNull(result.getBlueTeslas());

        verifyZeroInteractions(teslaService);
        verifyZeroInteractions(revenueService);
    }

    @Test
    public void testLowestprice_throwsException() throws Exception {
        when(priceService.findLowestPriceCars(anyListOf(Car.class))).thenThrow(new RentalCarException(123, "testException"));

        try {
            controller.lowestprice(rentalCarRequest);
            fail("Should have thrown exception");
        } catch (RentalCarException e) {
            assertEquals(123, e.getErrorId());

            verify(priceService, times(1)).findLowestPriceCars(carList);
            verifyZeroInteractions(teslaService);
            verifyZeroInteractions(revenueService);
        }
    }


    @Test
    public void testLowPriceDiscount_successful() throws Exception {
        when(priceService.findLowestPriceDiscountedCars(anyListOf(Car.class))).thenReturn(carList);

        RentalCarResponse result = controller.lowpricediscount(rentalCarRequest);

        verify(priceService, times(1)).findLowestPriceDiscountedCars(carList);

        assertEquals(carList, result.getLowestPriceDiscountedCars());
        assertNull(result.getLowestPriceCars());
        assertNull(result.getBlueTeslas());

        verifyZeroInteractions(teslaService);
        verifyZeroInteractions(revenueService);
    }

    @Test
    public void testLowPriceDiscount_throwsException() throws Exception {
        when(priceService.findLowestPriceDiscountedCars(anyListOf(Car.class))).thenThrow(new RentalCarException(123, "testException"));

        try {
            controller.lowpricediscount(rentalCarRequest);
            fail("Should have thrown exception");
        } catch (RentalCarException e) {
            assertEquals(123, e.getErrorId());

            verify(priceService, times(1)).findLowestPriceDiscountedCars(carList);
            verifyZeroInteractions(teslaService);
            verifyZeroInteractions(revenueService);
        }
    }

    @Test
    public void testHighprofit_successful() throws Exception {
        when(revenueService.findHighProfitCars(anyListOf(Car.class))).thenReturn(carList);

        RentalCarResponse result = controller.highprofit(rentalCarRequest);

        verify(revenueService, times(1)).findHighProfitCars(carList);

        assertEquals(carList, result.getHighProfitCars());
        assertNull(result.getLowestPriceDiscountedCars());
        assertNull(result.getLowestPriceCars());
        assertNull(result.getBlueTeslas());

        verifyZeroInteractions(teslaService);
        verifyZeroInteractions(priceService);
    }

    @Test
    public void testHighprofit_throwsException() throws Exception {
        when(revenueService.findHighProfitCars(anyListOf(Car.class))).thenThrow(new RentalCarException(123, "testException"));

        try {
            controller.highprofit(rentalCarRequest);
            fail("Should have thrown exception");
        } catch (RentalCarException e) {
            assertEquals(123, e.getErrorId());

            verify(revenueService, times(1)).findHighProfitCars(carList);
            verifyZeroInteractions(teslaService);
            verifyZeroInteractions(priceService);
        }
    }

    @Test
    public void testRentalservice_successful() throws Exception {
        when(teslaService.findTeslaCars(anyListOf(Car.class))).thenReturn(null);
        when(priceService.findLowestPriceCars(anyListOf(Car.class))).thenReturn(carList);
        when(priceService.findLowestPriceDiscountedCars(anyListOf(Car.class))).thenReturn(carList);
        when(revenueService.findHighProfitCars(anyListOf(Car.class))).thenReturn(carList);

        RentalCarResponse result = controller.rentalservice(rentalCarRequest);

        verify(teslaService).findTeslaCars(carList);
        verify(priceService).findLowestPriceCars(carList);
        verify(priceService).findLowestPriceDiscountedCars(carList);
        verify(revenueService, times(1)).findHighProfitCars(carList);

        assertNull(result.getBlueTeslas());
        assertEquals(carList, result.getLowestPriceCars());
        assertEquals(carList, result.getLowestPriceDiscountedCars());
        assertEquals(carList, result.getHighProfitCars());
    }
}
