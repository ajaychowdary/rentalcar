package com.sample.services;

import com.sample.exceptions.RentalCarException;
import com.sample.model.Car;
import com.sample.model.CarMetaData;
import com.sample.model.Perdayrent;
import com.sample.model.request.Metrics;
import com.sample.model.request.RentalCount;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class RevenueServiceTest {

    @InjectMocks
    private RevenueService revenueService;

    @Mock
    private ValidatorService validatorService;

    private List<Car> carList = null;

    @Before
    public void setUp() throws Exception {
        Car car = new Car();
        car.setMake("BMW");
        car.setModel("X3");
        car.setVin("abc123");

        CarMetaData metaData = new CarMetaData();
        metaData.setColor("white");
        metaData.setNotes("new");
        car.setMetadata(metaData);

        Metrics metrics = new Metrics();
        metrics.setDepreciation(45.56F);
        metrics.setYoymaintenancecost(556.77F);

        RentalCount rentalCount = new RentalCount();
        rentalCount.setLastweek(9);
        rentalCount.setYeartodate(99);

        metrics.setRentalCount(rentalCount);
        car.setMetrics(metrics);

        Perdayrent rent = new Perdayrent();
        rent.setDiscount(10F);
        rent.setPrice(60F);
        car.setPerdayrent(rent);

        Car car2 = new Car();
        car2.setMake("tesla");
        car2.setModel("Model X");
        car2.setVin("123");

        CarMetaData metaData2 = new CarMetaData();
        metaData2.setColor("blue");
        metaData2.setNotes("2019");
        car2.setMetadata(metaData2);

        Metrics metrics2 = new Metrics();
        metrics2.setDepreciation(12.23F);
        metrics2.setYoymaintenancecost(10.45F);

        RentalCount rentalCount2 = new RentalCount();
        rentalCount2.setLastweek(1);
        rentalCount2.setYeartodate(20);

        metrics2.setRentalCount(rentalCount2);
        car2.setMetrics(metrics2);

        Perdayrent rent2 = new Perdayrent();
        rent2.setDiscount(0.11F);
        rent2.setPrice(40.11F);
        car2.setPerdayrent(rent2);

        carList = new ArrayList<>();
        carList.add(car2);
        carList.add(car);
    }

    @Test
    public void findHighProfitCars() {
    }

    @Test
    public void findHighProfitCars_successful() throws RentalCarException {
        List<Car> result = revenueService.findHighProfitCars(carList);

        assertEquals(1, result.size());
        assertEquals("abc123", result.get(0).getVin());
        assertEquals("60.0", result.get(0).getPerdayrent().getPrice().toString());
        assertEquals("556.77", result.get(0).getMetrics().getYoymaintenancecost().toString());
    }

    @Test
    public void findHighProfitCars_returnTwoCars() throws RentalCarException {
        Car car = new Car();
        car.setMake("volvo");
        car.setModel("XC 90");
        car.setVin("xyz");

        CarMetaData metaData = new CarMetaData();
        metaData.setColor("white");
        metaData.setNotes("new");
        car.setMetadata(metaData);

        Metrics metrics = new Metrics();
        metrics.setDepreciation(245.23F);
        metrics.setYoymaintenancecost(2314.60F);

        RentalCount rentalCount = new RentalCount();
        rentalCount.setLastweek(1);
        rentalCount.setYeartodate(90);

        metrics.setRentalCount(rentalCount);
        car.setMetrics(metrics);

        Perdayrent rent = new Perdayrent();
        rent.setDiscount(13.25F);
        rent.setPrice(90F);
        car.setPerdayrent(rent);

        carList.add(car);

        List<Car> result = revenueService.findHighProfitCars(carList);

        assertEquals(2, result.size());
    }

}
