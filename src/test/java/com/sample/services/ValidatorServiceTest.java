package com.sample.services;

import com.sample.constants.RentalCarErrorCodes;
import com.sample.exceptions.RentalCarException;
import com.sample.model.Car;
import com.sample.model.CarMetaData;
import com.sample.model.Perdayrent;
import com.sample.model.request.Metrics;
import com.sample.model.request.RentalCarRequest;
import com.sample.model.request.RentalCount;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ValidatorServiceTest {

    @InjectMocks private ValidatorService service;

    private List<Car> carList = null;

    @Before
    public void setUp() throws Exception {
        Car car = new Car();
        car.setMake("BMW");
        car.setModel("X3");
        car.setVin("abc123");

        CarMetaData metaData = new CarMetaData();
        metaData.setColor("white");
        metaData.setNotes("new");
        car.setMetadata(metaData);

        Metrics metrics = new Metrics();
        metrics.setDepreciation(12.23F);
        metrics.setYoymaintenancecost(23.45F);

        RentalCount rentalCount = new RentalCount();
        rentalCount.setLastweek(1);
        rentalCount.setYeartodate(20);

        metrics.setRentalCount(rentalCount);
        car.setMetrics(metrics);

        Perdayrent rent = new Perdayrent();
        rent.setDiscount(10F);
        rent.setPrice(50F);
        car.setPerdayrent(rent);

        Car car2 = new Car();
        car2.setMake("volvo");
        car2.setModel("XC 90");
        car2.setVin("123");

        CarMetaData metaData2 = new CarMetaData();
        metaData2.setColor("white");
        metaData2.setNotes("2019");
        car2.setMetadata(metaData2);

        Metrics metrics2 = new Metrics();
        metrics2.setDepreciation(12.23F);
        metrics2.setYoymaintenancecost(23.45F);

        RentalCount rentalCount2 = new RentalCount();
        rentalCount2.setLastweek(1);
        rentalCount2.setYeartodate(20);

        metrics2.setRentalCount(rentalCount2);
        car2.setMetrics(metrics2);

        Perdayrent rent2 = new Perdayrent();
        rent2.setDiscount(10F);
        rent2.setPrice(50F);
        car2.setPerdayrent(rent2);

        carList = new ArrayList<>();
        carList.add(car);
        carList.add(car2);
    }

    @Test
    public void performValidationsOnMakeAndMetaData_EmptyVin() {
        Car car = carList.get(1);
        car.setVin(null);

        try {
            service.performValidationsOnMakeAndMetaData(carList);
        } catch (RentalCarException e) {
            assertEquals(RentalCarErrorCodes.VALIDATION_FAILURE.getErrorCode(), e.getErrorId());
            assertEquals("Car VIN information can not be empty.", e.getMessage());
        }
    }

    @Test
    public void performValidationsOnMakeAndMetaData_EmptyMake() {
        Car car = carList.get(1);
        car.setMake("  ");

        try {
            service.performValidationsOnMakeAndMetaData(carList);
        } catch (RentalCarException e) {
            assertEquals(RentalCarErrorCodes.VALIDATION_FAILURE.getErrorCode(), e.getErrorId());
            assertEquals("Car Make information can not be empty for the vehicle with VIN: 123", e.getMessage());
        }
    }

    @Test
    public void performValidationsOnMakeAndMetaData_EmptyMetaData() {
        Car car = carList.get(1);
        car.setMetadata(null);

        try {
            service.performValidationsOnMakeAndMetaData(carList);
        } catch (RentalCarException e) {
            assertEquals(RentalCarErrorCodes.VALIDATION_FAILURE.getErrorCode(), e.getErrorId());
            assertEquals("Car Metadata information can not be empty for the vehicle with VIN: 123", e.getMessage());
        }
    }

    @Test
    public void performValidationsOnMakeAndMetaData_EmptyColor() {
        Car car = carList.get(1);
        CarMetaData metaData = car.getMetadata();
        metaData.setColor("");

        try {
            service.performValidationsOnMakeAndMetaData(carList);
        } catch (RentalCarException e) {
            assertEquals(RentalCarErrorCodes.VALIDATION_FAILURE.getErrorCode(), e.getErrorId());
            assertEquals("Car Color information can not be empty for the vehicle with VIN: 123", e.getMessage());
        }
    }

    @Test
    public void performValidationsOnMakeAndMetaData_EmptyNotes() {
        Car car = carList.get(1);
        CarMetaData metaData = car.getMetadata();
        metaData.setNotes(" ");

        try {
            service.performValidationsOnMakeAndMetaData(carList);
        } catch (RentalCarException e) {
            assertEquals(RentalCarErrorCodes.VALIDATION_FAILURE.getErrorCode(), e.getErrorId());
            assertEquals("Car Notes information can not be empty for the vehicle with VIN: 123", e.getMessage());
        }
    }

    @Test
    public void performValidationsOnMakeAndMetaData_Successful() {
        try {
            service.performValidationsOnMakeAndMetaData(carList);
        } catch (RentalCarException e) {
            fail("Should not throw any exception, failed with message: " +e.getMessage());
        }
    }

    @Test
    public void performValidationsOnMakeAndMetaData_EmptyRequest() {
        try {
            service.performValidationsOnMakeAndMetaData(null);
        } catch (RentalCarException e) {
            assertEquals(RentalCarErrorCodes.INVALID_REQUEST.getErrorCode(), e.getErrorId());
            assertEquals("Cars in RentalCarRequest are empty, provide cars information.", e.getMessage());
        }
    }


    @Test
    public void performValidationsOnPrice_EmptyPerdayrent() {
        Car car = carList.get(1);
        car.setPerdayrent(null);

        try {
            service.performValidationsOnPrice(carList);
        } catch (RentalCarException e) {
            assertEquals(RentalCarErrorCodes.VALIDATION_FAILURE.getErrorCode(), e.getErrorId());
            assertEquals("Car Per Day Rent information can not be empty for the vehicle with VIN: 123", e.getMessage());
        }
    }

    @Test
    public void performValidationsOnPrice_ZeroPrice() {
        Car car = carList.get(1);
        Perdayrent rent = car.getPerdayrent();
        rent.setPrice(0F);

        try {
            service.performValidationsOnPrice(carList);
        } catch (RentalCarException e) {
            assertEquals(RentalCarErrorCodes.VALIDATION_FAILURE.getErrorCode(), e.getErrorId());
            assertEquals("Car price information is not provided for the vehicle with VIN: 123", e.getMessage());
        }
    }

    @Test
    public void performValidationsOnPrice_Successful() {
        try {
            service.performValidationsOnPrice(carList);
        } catch (RentalCarException e) {
            fail("Should not throw any exception, failed with message: " +e.getMessage());

        }
    }

    @Test
    public void performValidationsOnMetrics_ZeroPrice() {
        Car car = carList.get(1);
        Perdayrent rent = car.getPerdayrent();
        rent.setPrice(0F);

        try {
            service.performValidationsOnMetrics(carList);
        } catch (RentalCarException e) {
            assertEquals(RentalCarErrorCodes.VALIDATION_FAILURE.getErrorCode(), e.getErrorId());
            assertEquals("Car price information is not provided for the vehicle with VIN: 123", e.getMessage());
        }
    }

    @Test
    public void performValidationsOnMetrics_Successful() {
        try {
            service.performValidationsOnMetrics(carList);
        } catch (RentalCarException e) {
            fail("Should not throw any exception, failed with message: " +e.getMessage());

        }
    }
}
