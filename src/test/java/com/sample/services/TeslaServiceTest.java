package com.sample.services;

import com.sample.exceptions.RentalCarException;
import com.sample.model.Car;
import com.sample.model.CarMetaData;
import com.sample.model.Perdayrent;
import com.sample.model.request.Metrics;
import com.sample.model.request.RentalCount;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class TeslaServiceTest {

    @InjectMocks private TeslaService teslaService;
    @Mock private ValidatorService validatorService;

    private List<Car> carList = null;

    @Before
    public void setUp() throws Exception {
        Car car = new Car();
        car.setMake("BMW");
        car.setModel("X3");
        car.setVin("abc123");

        CarMetaData metaData = new CarMetaData();
        metaData.setColor("white");
        metaData.setNotes("new");
        car.setMetadata(metaData);

        Metrics metrics = new Metrics();
        metrics.setDepreciation(12.23F);
        metrics.setYoymaintenancecost(23.45F);

        RentalCount rentalCount = new RentalCount();
        rentalCount.setLastweek(1);
        rentalCount.setYeartodate(20);

        metrics.setRentalCount(rentalCount);
        car.setMetrics(metrics);

        Perdayrent rent = new Perdayrent();
        rent.setDiscount(10F);
        rent.setPrice(50F);
        car.setPerdayrent(rent);

        Car car2 = new Car();
        car2.setMake("tesla");
        car2.setModel("Model X");
        car2.setVin("123");

        CarMetaData metaData2 = new CarMetaData();
        metaData2.setColor("blue");
        metaData2.setNotes("2019");
        car2.setMetadata(metaData2);

        Metrics metrics2 = new Metrics();
        metrics2.setDepreciation(12.23F);
        metrics2.setYoymaintenancecost(23.45F);

        RentalCount rentalCount2 = new RentalCount();
        rentalCount2.setLastweek(1);
        rentalCount2.setYeartodate(20);

        metrics2.setRentalCount(rentalCount2);
        car2.setMetrics(metrics2);

        Perdayrent rent2 = new Perdayrent();
        rent2.setDiscount(10F);
        rent2.setPrice(50F);
        car2.setPerdayrent(rent2);

        carList = new ArrayList<>();
        carList.add(car);
        carList.add(car2);
    }

    @Test
    public void findTeslaCars_successful() throws RentalCarException {

        List<Car> result = teslaService.findTeslaCars(carList);

        assertEquals(1, result.size());
        assertEquals("123", result.get(0).getVin());
        assertEquals("tesla", result.get(0).getMake());
        assertEquals("blue", result.get(0).getMetadata().getColor());
        assertEquals("2019", result.get(0).getMetadata().getNotes());
    }

    @Test
    public void findTeslaCars_NoBlueTeslas() throws RentalCarException {
        Car car = carList.get(1);
        CarMetaData metaData = car.getMetadata();
        metaData.setColor("red");

        List<Car> result = teslaService.findTeslaCars(carList);

        assertEquals(0, result.size());
    }
}
