package com.sample.services;

import com.sample.exceptions.RentalCarException;
import com.sample.model.Car;
import com.sample.model.CarMetaData;
import com.sample.model.Perdayrent;
import com.sample.model.request.Metrics;
import com.sample.model.request.RentalCount;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class PriceServiceTest {

    @InjectMocks private PriceService priceService;
    @Mock private ValidatorService validatorService;

    private List<Car> carList = null;

    @Before
    public void setUp() throws Exception {
        Car car = new Car();
        car.setMake("BMW");
        car.setModel("X3");
        car.setVin("abc123");

        CarMetaData metaData = new CarMetaData();
        metaData.setColor("white");
        metaData.setNotes("new");
        car.setMetadata(metaData);

        Metrics metrics = new Metrics();
        metrics.setDepreciation(12.23F);
        metrics.setYoymaintenancecost(23.45F);

        RentalCount rentalCount = new RentalCount();
        rentalCount.setLastweek(1);
        rentalCount.setYeartodate(20);

        metrics.setRentalCount(rentalCount);
        car.setMetrics(metrics);

        Perdayrent rent = new Perdayrent();
        rent.setDiscount(10F);
        rent.setPrice(50F);
        car.setPerdayrent(rent);

        Car car2 = new Car();
        car2.setMake("tesla");
        car2.setModel("Model X");
        car2.setVin("123");

        CarMetaData metaData2 = new CarMetaData();
        metaData2.setColor("blue");
        metaData2.setNotes("2019");
        car2.setMetadata(metaData2);

        Metrics metrics2 = new Metrics();
        metrics2.setDepreciation(12.23F);
        metrics2.setYoymaintenancecost(23.45F);

        RentalCount rentalCount2 = new RentalCount();
        rentalCount2.setLastweek(1);
        rentalCount2.setYeartodate(20);

        metrics2.setRentalCount(rentalCount2);
        car2.setMetrics(metrics2);

        Perdayrent rent2 = new Perdayrent();
        rent2.setDiscount(0.12F);
        rent2.setPrice(40.12F);
        car2.setPerdayrent(rent2);

        carList = new ArrayList<>();
        carList.add(car);
        carList.add(car2);
    }

    @Test
    public void findLowestPriceCars_successful() throws RentalCarException {
        List<Car> result = priceService.findLowestPriceCars(carList);

        assertEquals(1, result.size());
        assertEquals("123", result.get(0).getVin());
        assertEquals("40.12", result.get(0).getPerdayrent().getPrice().toString());
        assertEquals("0.12", result.get(0).getPerdayrent().getDiscount().toString());
    }

    @Test
    public void findLowestPriceCars_returnTwoCars() throws RentalCarException {
        Car car = new Car();
        car.setMake("volvo");
        car.setModel("XC 90");
        car.setVin("xyz");

        CarMetaData metaData = new CarMetaData();
        metaData.setColor("white");
        metaData.setNotes("new");
        car.setMetadata(metaData);

        Metrics metrics = new Metrics();
        metrics.setDepreciation(12.23F);
        metrics.setYoymaintenancecost(23.45F);

        RentalCount rentalCount = new RentalCount();
        rentalCount.setLastweek(1);
        rentalCount.setYeartodate(20);

        metrics.setRentalCount(rentalCount);
        car.setMetrics(metrics);

        Perdayrent rent = new Perdayrent();
        rent.setDiscount(10F);
        rent.setPrice(40.12F);
        car.setPerdayrent(rent);

        carList.add(car);

        List<Car> result = priceService.findLowestPriceCars(carList);

        assertEquals(2, result.size());
    }

    @Test
    public void findLowestPriceDiscountedCars_successful() throws RentalCarException {
        List<Car> result = priceService.findLowestPriceDiscountedCars(carList);

        assertEquals(2, result.size());
    }

    @Test
    public void findLowestPriceDiscountedCars_returnTwoCars() throws RentalCarException {
        Car car = carList.get(0);
        Perdayrent perdayrent = car.getPerdayrent();
        perdayrent.setDiscount(15F);

        List<Car> result = priceService.findLowestPriceDiscountedCars(carList);

        assertEquals(1, result.size());
        assertEquals("abc123", result.get(0).getVin());
        assertEquals("50.0", result.get(0).getPerdayrent().getPrice().toString());
        assertEquals("15.0", result.get(0).getPerdayrent().getDiscount().toString());
    }
}
