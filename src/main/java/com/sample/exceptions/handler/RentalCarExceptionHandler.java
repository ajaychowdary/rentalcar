package com.sample.exceptions.handler;

import com.sample.constants.RentalCarErrorCodes;
import com.sample.exceptions.RentalCarException;
import com.sample.model.response.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.ws.rs.core.Response;


@ControllerAdvice
public class RentalCarExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger LOG = LoggerFactory.getLogger(RentalCarExceptionHandler.class);

    @ExceptionHandler(RentalCarException.class)
    public ResponseEntity<ErrorResponse> handleRentalCarException(RentalCarException ex, WebRequest request) {

        int responseCode;
        long errorCode = ex.getErrorId();

        if (errorCode == RentalCarErrorCodes.INVALID_REQUEST.getErrorCode() ||
                errorCode == RentalCarErrorCodes.VALIDATION_FAILURE.getErrorCode()) {
            responseCode = Response.Status.BAD_REQUEST.getStatusCode();
        } else {
            responseCode = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
        }

        ErrorResponse response = new ErrorResponse();
        response.setErrorId(ex.getErrorId());
        response.setErrorMessage(ex.getMessage());
        response.setStatus(responseCode);

        LOG.info("Rental Car Service Error response: [errorId: [{}], errorMessage: [{}]", response.getErrorId(), response.getErrorMessage());

         return  new ResponseEntity<>(response, HttpStatus.valueOf(responseCode));
    }
}