package com.sample.exceptions;

import com.sample.constants.RentalCarErrorCodes;

public class RentalCarException extends Exception {

    private long errorId;
    private String message;
    private RentalCarErrorCodes errorCode;

    public RentalCarException(long errorId, String message) {
        super(message);
        this.errorId = errorId;
        this.message = message;
    }

    public RentalCarException(Throwable cause, long errorId, String message) {
        super(message, cause);
        this.errorId = errorId;
        this.message = message;
    }

    public RentalCarException(RentalCarErrorCodes errorCode) {
        super(errorCode.getMessage());
        this.errorId = errorCode.getErrorCode();
        this.message = errorCode.getMessage();
    }

    public RentalCarException(Throwable cause, RentalCarErrorCodes errorCode) {
        super(cause);
        this.errorId = errorCode.getErrorCode();
        this.message = errorCode.getMessage();
    }

    public long getErrorId() {
        return errorId;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
