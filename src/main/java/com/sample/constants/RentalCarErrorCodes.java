package com.sample.constants;

public enum RentalCarErrorCodes {

    INVALID_REQUEST(300001, "Invalid Request: RentalCarRequest is invalid, provide valid details."),
    VALIDATION_FAILURE(300002, "RentalCarRequest Validation Failure");

    private long errorCode;
    private String message;

    RentalCarErrorCodes(long errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public long getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }
}
