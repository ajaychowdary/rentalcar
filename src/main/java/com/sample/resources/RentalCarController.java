package com.sample.resources;


import com.sample.exceptions.RentalCarException;
import com.sample.model.request.RentalCarRequest;
import com.sample.model.response.RentalCarResponse;
import com.sample.services.PriceService;
import com.sample.services.RevenueService;
import com.sample.services.TeslaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@RestController
public class RentalCarController {
    private static final Logger LOG = LoggerFactory.getLogger(RentalCarController.class);

    @Inject
    private TeslaService teslaService;

    @Inject
    private PriceService priceService;

    @Inject
    private RevenueService revenueService;

    /**
     * This method returns all the blue teslas car information from the given cars.
     * @param  rentalCarRequest
     * @throws RentalCarException
     * @return list of blue tesla cars
     *
    **/
    @POST
    @RequestMapping("/tesla")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public RentalCarResponse teslaCars(@RequestBody RentalCarRequest rentalCarRequest) throws RentalCarException {
        LOG.info("Received request to find TESLA cars");

        RentalCarResponse response = new RentalCarResponse();
        response.setBlueTeslas(teslaService.findTeslaCars(rentalCarRequest.getCars()));

        LOG.info("Completed Blue Tesla Cars lookup, sending response...");
        return response;
    }

    /**
     * This method returns all the cars having low prices and lowest price after discounts from the given cars.
     * @param  rentalCarRequest
     * @throws RentalCarException
     * @return list of low priced cars and list of low priced cars after discounts
     *
     **/
    @POST
    @RequestMapping("/lowprice")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public RentalCarResponse lowprice(@RequestBody RentalCarRequest rentalCarRequest) throws RentalCarException {
        LOG.info("Received request to find low priced cars");

        RentalCarResponse rentalCarResponse = new RentalCarResponse();
        rentalCarResponse.setLowestPriceCars(priceService.findLowestPriceCars(rentalCarRequest.getCars()));
        rentalCarResponse.setLowestPriceDiscountedCars(priceService.findLowestPriceDiscountedCars(rentalCarRequest.getCars()));

        LOG.info("Completed low priced cars lookup, sending response...");
        return rentalCarResponse;
    }

    /**
     * This method returns all the cars having lowest price from the given cars.
     * @param  rentalCarRequest
     * @throws RentalCarException
     * @return list of low priced cars
     *
     **/
    @POST
    @RequestMapping("/lowestprice")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public RentalCarResponse lowestprice(@RequestBody RentalCarRequest rentalCarRequest) throws RentalCarException {
        LOG.info("Received request to find lowest priced cars");

        RentalCarResponse response = new RentalCarResponse();
        response.setLowestPriceCars(priceService.findLowestPriceCars(rentalCarRequest.getCars()));

        LOG.info("Completed lowest priced cars lookup, sending response...");
        return response;
    }

    /**
     * This method returns all the cars having lowest price after discounts from the given cars.
     * @param  rentalCarRequest
     * @throws RentalCarException
     * @return list of low priced cars after discounts
     *
     **/
    @POST
    @RequestMapping("/lowpricediscount")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public RentalCarResponse lowpricediscount(@RequestBody RentalCarRequest rentalCarRequest) throws RentalCarException {
        LOG.info("Received request to find cars with lowest price after discounts");

        RentalCarResponse response = new RentalCarResponse();
        response.setLowestPriceDiscountedCars(priceService.findLowestPriceDiscountedCars(rentalCarRequest.getCars()));

        LOG.info("Completed lookup for cars with lowest price after discounts, sending response...");
        return response;
    }


    /**
     * This method returns all the cars having high revenue from the given cars.
     * @param  rentalCarRequest
     * @throws RentalCarException
     * @return list of high revenue cars
     *
     **/
    @POST
    @RequestMapping("/highprofit")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public RentalCarResponse highprofit(@RequestBody RentalCarRequest rentalCarRequest) throws RentalCarException {
        LOG.info("Received request to find high profit cars");

        RentalCarResponse response = new RentalCarResponse();
        response.setHighProfitCars(revenueService.findHighProfitCars(rentalCarRequest.getCars()));

        LOG.info("Completed lookup for high profit cars, sending response...");
        return response;
    }

    /**
     * This method returns all the blue tesla's, cars having lowest price, lowest price after discounts and cars having highest revenue.
     * @param  rentalCarRequest
     * @throws RentalCarException
     * @return list of cars
     *
     **/
    @POST
    @RequestMapping("/rentalservice")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public RentalCarResponse rentalservice(@RequestBody RentalCarRequest rentalCarRequest) throws RentalCarException {
        LOG.info("Received request for rental service");

        RentalCarResponse response = new RentalCarResponse();
        response.setBlueTeslas(teslaService.findTeslaCars(rentalCarRequest.getCars()));
        response.setLowestPriceCars(priceService.findLowestPriceCars(rentalCarRequest.getCars()));
        response.setLowestPriceDiscountedCars(priceService.findLowestPriceDiscountedCars(rentalCarRequest.getCars()));
        response.setHighProfitCars(revenueService.findHighProfitCars(rentalCarRequest.getCars()));

        LOG.info("Completed rental service, sending response...");
        return response;
    }
}
