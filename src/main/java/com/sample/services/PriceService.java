package com.sample.services;

import com.sample.exceptions.RentalCarException;
import com.sample.model.Car;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.*;

@Component
public class PriceService {
    private static final Logger LOG = LoggerFactory.getLogger(PriceService.class);

    @Inject
    private ValidatorService validatorService;


    /**
     * This method returns List of cars with lowest price.
     * @param rentalCars
     * @return List<Car>
     * @throws RentalCarException
     */
    public List<Car> findLowestPriceCars(List<Car> rentalCars) throws RentalCarException {
        validatorService.performValidationsOnPrice(rentalCars);
        LOG.info("Finding Lowest priced Cars in the given rental cars list...");

        Map<Car, Float> carLowPriceMap = new HashMap<>();
        for (Car car : rentalCars) {
            float price = car.getPerdayrent().getPrice().floatValue();
            LOG.debug("Car VIN:[{}], Price:[{}]", car.getVin(), price);

            carLowPriceMap.put(car, new Float(price));
        }

        List<Car> lowestPriceCars = getLowPriceCars(carLowPriceMap);
        LOG.info("Done finding lowest priced cars in given cars list.");

        return lowestPriceCars;
    }

    /**
     * This method returns all the cars having low prices and lowest price after discounts from the given cars.
     * @param rentalCars
     * @return List<Car>
     * @throws RentalCarException
     */
    public List<Car> findLowestPriceDiscountedCars(List<Car> rentalCars) throws RentalCarException {
        validatorService.performValidationsOnPrice(rentalCars);
        LOG.info("Finding cars with low price after discounts in the given rental cars list...");

        Map<Car, Float> carLowPriceDiscountMap = new HashMap<>();
        for (Car car : rentalCars) {
            float price = car.getPerdayrent().getPrice().floatValue();
            if (car.getPerdayrent().getDiscount() != null) {
                price = price - car.getPerdayrent().getDiscount().floatValue();
            }
            LOG.debug("Car VIN:[{}], Price after Discount:[{}]", car.getVin(), price);

            carLowPriceDiscountMap.put(car, new Float(price));
        }

        List<Car> lowestPriceCars = getLowPriceCars(carLowPriceDiscountMap);
        LOG.info("Done finding cars with lowe price after discounts in given cars list.");

        return lowestPriceCars;
    }

    /**
     * This method returns all the cars having low prices.
     * @param carPriceMap
     * @return List<Car>
     */
    private List<Car> getLowPriceCars(Map<Car, Float> carPriceMap) {
        List<Car> lowPriceCars = new ArrayList<>();

        Iterator<Map.Entry<Car, Float>> iterator = carPriceMap.entrySet().iterator();

        Map.Entry<Car, Float> firstCar = iterator.next();
        float lowestPrice = firstCar.getValue().floatValue();
        lowPriceCars.add(firstCar.getKey());

        while (iterator.hasNext()) {
            Map.Entry<Car, Float> car = iterator.next();
            float carPrice = car.getValue().floatValue();

            if (Float.compare(carPrice, lowestPrice) == 0) {
                lowPriceCars.add(car.getKey());
            } else if (Float.compare(carPrice, lowestPrice) < 0) {
                lowestPrice = carPrice;
                lowPriceCars.clear();
                lowPriceCars.add(car.getKey());
            }
        }

        return lowPriceCars;
    }
}
