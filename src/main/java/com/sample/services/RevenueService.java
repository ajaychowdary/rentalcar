package com.sample.services;

import com.sample.exceptions.RentalCarException;
import com.sample.model.Car;
import com.sample.model.request.Metrics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.*;

@Component
public class RevenueService {
    private static final Logger LOG = LoggerFactory.getLogger(RevenueService.class);

    @Inject
    private ValidatorService validatorService;

    /**
     * This method returns all the cars having high revenue from the given cars.
     * @param rentalCars
     * @return List<Car>
     * @throws RentalCarException
     */
    public List<Car> findHighProfitCars(List<Car> rentalCars) throws RentalCarException {
        validatorService.performValidationsOnMetrics(rentalCars);
        LOG.info("Finding high profit cars in the given rental cars list...");

        Map<Car, Float> carPriceMap = new HashMap<>();
        for (Car car : rentalCars) {
            float profitPrice = getCarProfit(car);
            LOG.debug("Car VIN:[{}], profitMoney:[{}]", car.getVin(), profitPrice);

            carPriceMap.put(car, new Float(profitPrice));
        }

        List<Car> lowestPriceCars = getHighProfitCars(carPriceMap);
        LOG.info("Done finding high profit cars in given cars list.");

        return lowestPriceCars;
    }

    /**
     * This method returns the car profit.
     * @param car
     * @return car profit
     */
    private float getCarProfit(Car car) {
        float totalExpenses = 0;
        int totalRentals = 0;

        /* Considering the price after discounts as the price for the rental */
        float pricePerRental = car.getPerdayrent().getPrice().floatValue();
        if (car.getPerdayrent().getDiscount() != null) {
            pricePerRental = pricePerRental - car.getPerdayrent().getDiscount().floatValue();
        }

        Metrics metrics = car.getMetrics();
        if (metrics != null) {
            totalExpenses = metrics.getYoymaintenancecost() + metrics.getDepreciation();

            if (metrics.getRentalCount() != null) {
                totalRentals = metrics.getRentalCount().getYeartodate();
            }
        }

        return (totalRentals * pricePerRental) - totalExpenses;
    }

    /**
     * This method returns list of cars with high profit
     * @param carPriceMap
     * @return List<Car>
     */
    private List<Car> getHighProfitCars(Map<Car, Float> carPriceMap) {
        Iterator<Map.Entry<Car, Float>> iterator = carPriceMap.entrySet().iterator();
        Map.Entry<Car, Float> firstCar = iterator.next();

        List<Car> highProfitCars = new ArrayList<>();
        float highestProfit = firstCar.getValue().floatValue();
        highProfitCars.add(firstCar.getKey());

        while (iterator.hasNext()) {
            Map.Entry<Car, Float> car = iterator.next();
            float carPrice = car.getValue().floatValue();

            if (Float.compare(carPrice, highestProfit) == 0) {
                highProfitCars.add(car.getKey());
            } else if (Float.compare(carPrice, highestProfit) > 0) {
                highestProfit = carPrice;
                highProfitCars.clear();
                highProfitCars.add(car.getKey());
            }
        }

        return highProfitCars;
    }
}
