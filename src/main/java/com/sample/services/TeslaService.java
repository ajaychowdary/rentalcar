package com.sample.services;

import com.sample.exceptions.RentalCarException;
import com.sample.model.Car;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static com.sample.constants.RentalCarConstants.BLUE_TESLA;
import static com.sample.constants.RentalCarConstants.TESLA;

@Component
public class TeslaService {
    private static final Logger LOG = LoggerFactory.getLogger(TeslaService.class);

    @Inject
    private ValidatorService validatorService;

    /**
     * This method returns all the blue tesla cars along with notes
     * Notes and other Information can be seen in logs too.
     * @param rentalCars
     * @return List<Car>
     * @throws RentalCarException
     */
    public List<Car> findTeslaCars(List<Car> rentalCars) throws RentalCarException {
        validatorService.performValidationsOnMakeAndMetaData(rentalCars);
        LOG.info("Finding Blue Tesla Cars in the given rental cars list...");

        List<Car> teslaCars = new ArrayList<>();
        for (Car car : rentalCars) {
            if (TESLA.equalsIgnoreCase(car.getMake()) && BLUE_TESLA.equalsIgnoreCase(car.getMetadata().getColor())) {
                LOG.info("*** Found Blue Tesla with VIN:[{}], Model:[{}], notes:[{}] ***", car.getVin(), car.getModel(), car.getMetadata().getNotes());

                teslaCars.add(car);
            }
        }
        LOG.info("Done finding Blue Tesla's in given cars list, found [{}] cars.", teslaCars.size());

        return teslaCars;
    }
}
