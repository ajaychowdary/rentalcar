package com.sample.services;

import com.sample.constants.RentalCarErrorCodes;
import com.sample.exceptions.RentalCarException;
import com.sample.model.Car;
import com.sample.model.CarMetaData;
import com.sample.model.Perdayrent;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Component
public class ValidatorService {
    private static final Logger LOG = LoggerFactory.getLogger(ValidatorService.class);

    /**
     * This method performs validations for car make and metadata(color and notes)
     * @param request - List of cars
     * @throws RentalCarException
     */
    public void performValidationsOnMakeAndMetaData(List<Car> request) throws RentalCarException {
        LOG.info("performing validations on Make and MetaData information...");

        baseValidation(request);

        for (Car car: request) {
            String vin = getVINInfo(car.getVin());

            if (StringUtils.isBlank(car.getMake())) {
                LOG.error("Make information is empty for the car with VIN:[{}]", vin);

                throw new RentalCarException(RentalCarErrorCodes.VALIDATION_FAILURE.getErrorCode(),
                        "Car Make information can not be empty for the vehicle with VIN: "+vin);
            }

            CarMetaData metaData = car.getMetadata();
            if (metaData == null) {
                LOG.error("Metadata information is empty for the car with VIN:[{}]", vin);

                throw new RentalCarException(RentalCarErrorCodes.VALIDATION_FAILURE.getErrorCode(),
                        "Car Metadata information can not be empty for the vehicle with VIN: "+vin);
            }

            if (StringUtils.isBlank(metaData.getColor())) {
                LOG.error("Color information is empty for the car with VIN:[{}]", vin);

                throw new RentalCarException(RentalCarErrorCodes.VALIDATION_FAILURE.getErrorCode(),
                        "Car Color information can not be empty for the vehicle with VIN: "+vin);
            }

            if (StringUtils.isBlank(metaData.getNotes())) {
                LOG.error("Car Notes is empty for the car with VIN:[{}]", vin);

                throw new RentalCarException(RentalCarErrorCodes.VALIDATION_FAILURE.getErrorCode(),
                        "Car Notes information can not be empty for the vehicle with VIN: "+vin);
            }
        }

        LOG.info("Completed validations on Make and MetaData information.");
    }

    /**
     * This method performs validations for car per day rent(price)
     * Not performing validations on Discount field as there could be no discounts
     * @param request - List of cars
     * @throws RentalCarException
     */
    public void performValidationsOnPrice(List<Car> request) throws RentalCarException {
        LOG.info("performing validations on Price information...");

        baseValidation(request);

        for (Car car: request) {
            String vin = getVINInfo(car.getVin());

            Perdayrent perdayrent = car.getPerdayrent();
            if (perdayrent == null) {
                LOG.error("Per Day Rent information is empty for the car with VIN:[{}]", vin);

                throw new RentalCarException(RentalCarErrorCodes.VALIDATION_FAILURE.getErrorCode(),
                        "Car Per Day Rent information can not be empty for the vehicle with VIN: "+vin);
            }

            if (perdayrent.getPrice() == 0) {
                LOG.error("Car price is empty or zero for the car with VIN:[{}]", vin);

                throw new RentalCarException(RentalCarErrorCodes.VALIDATION_FAILURE.getErrorCode(),
                        "Car price information is not provided for the vehicle with VIN: "+vin);
            }

            /* Not performing validations on Discount field: as there could be no discounts */
        }

        LOG.info("Completed validations on Price information.");
    }

    /**
     * This method performs validations for car price
     * @param request - List of cars
     * @throws RentalCarException
     */
    public void performValidationsOnMetrics(List<Car> request) throws RentalCarException {
        LOG.info("performing validations on Metrics information...");

        performValidationsOnPrice(request);

        /*
           Considering price for each rental as mandatory and
           not performing validations on Metrics fields :
           consider new car is listed in the request, it will have a price on it to rent but it won't have any rental history, maintenance or depreciation
         */

        LOG.info("Completed validations on Metrics information.");
    }

    /**
     * This method throws exception if no vin provided else will return vin
     * @param vin
     * @return throws exception if no vin provided else will return vin
     * @throws RentalCarException
     */
    private String getVINInfo(String vin) throws RentalCarException{
        /* Considering VIN as the primary identifier for the car */

        if (StringUtils.isBlank(vin)) {
            LOG.error("Car VIN information in one of the cars is empty");

            throw new RentalCarException(RentalCarErrorCodes.VALIDATION_FAILURE.getErrorCode(),
                    "Car VIN information can not be empty.");
        }

        return vin;
    }

    /**
     * This method performs Validation for the request(list of cars)
     * @param request - List of cars
     * @throws RentalCarException
     */
    private void baseValidation(List<Car> request) throws RentalCarException {
        if (CollectionUtils.isEmpty(request)) {
            LOG.error("No Cars information is provided in RentalCarRequest.");

            throw new RentalCarException(RentalCarErrorCodes.INVALID_REQUEST.getErrorCode(),
                    "Cars in RentalCarRequest are empty, provide cars information.");
        }
    }
}
