package com.sample.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Metrics {

    private Float yoymaintenancecost;
    private Float depreciation;
    private RentalCount rentalCount;

    public Float getYoymaintenancecost() {
        return yoymaintenancecost;
    }

    public void setYoymaintenancecost(Float yoymaintenancecost) {
        this.yoymaintenancecost = yoymaintenancecost;
    }

    public Float getDepreciation() {
        return depreciation;
    }

    public void setDepreciation(Float depreciation) {
        this.depreciation = depreciation;
    }

    public RentalCount getRentalCount() {
        return rentalCount;
    }

    public void setRentalCount(RentalCount rentalCount) {
        this.rentalCount = rentalCount;
    }
}
