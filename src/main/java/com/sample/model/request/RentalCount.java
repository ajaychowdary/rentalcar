package com.sample.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RentalCount {

    private int lastweek;
    private int yeartodate;

    public int getLastweek() {
        return lastweek;
    }

    public void setLastweek(int lastweek) {
        this.lastweek = lastweek;
    }

    public int getYeartodate() {
        return yeartodate;
    }

    public void setYeartodate(int yeartodate) {
        this.yeartodate = yeartodate;
    }
}
