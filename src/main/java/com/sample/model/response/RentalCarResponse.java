package com.sample.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sample.model.Car;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RentalCarResponse {

    private List<Car> blueTeslas;
    private List<Car> lowestPriceCars;
    private List<Car> lowestPriceDiscountedCars;
    private List<Car> highProfitCars;

    public List<Car> getBlueTeslas() {
        return blueTeslas;
    }

    public void setBlueTeslas(List<Car> blueTeslas) {
        this.blueTeslas = blueTeslas;
    }

    public List<Car> getLowestPriceCars() {
        return lowestPriceCars;
    }

    public void setLowestPriceCars(List<Car> lowestPriceCars) {
        this.lowestPriceCars = lowestPriceCars;
    }

    public List<Car> getLowestPriceDiscountedCars() {
        return lowestPriceDiscountedCars;
    }

    public void setLowestPriceDiscountedCars(List<Car> lowestPriceDiscountedCars) {
        this.lowestPriceDiscountedCars = lowestPriceDiscountedCars;
    }

    public List<Car> getHighProfitCars() {
        return highProfitCars;
    }

    public void setHighProfitCars(List<Car> highProfitCars) {
        this.highProfitCars = highProfitCars;
    }
}
