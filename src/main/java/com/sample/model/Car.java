package com.sample.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sample.model.request.Metrics;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Car {

    private String make;
    private String model;
    private String vin;
    private CarMetaData metadata;
    private Perdayrent perdayrent;
    private Metrics metrics;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public CarMetaData getMetadata() {
        return metadata;
    }

    public void setMetadata(CarMetaData metadata) {
        this.metadata = metadata;
    }

    public Perdayrent getPerdayrent() {
        return perdayrent;
    }

    public void setPerdayrent(Perdayrent perdayrent) {
        this.perdayrent = perdayrent;
    }

    public Metrics getMetrics() {
        return metrics;
    }

    public void setMetrics(Metrics metrics) {
        this.metrics = metrics;
    }
}
